Fibre is a prototype for a lighter desktop client for Wire (https://wire.com/).

Instead of using Electron, it hosts the web application of Wire (https://app.wire.com/) in a web view contained in a thin native wrapper provided by Qt. As a result
it consumes 1/5 of the memory needed by the official desktop client for Wire (https://github.com/wireapp/wire-desktop/) and less power as well.

This project has been started as a proof of concept. If it proves itself as reliable and there's enough interest by the community, the Wire team might consider
switching to it for its desktop offers thus benefiting all of us users.
