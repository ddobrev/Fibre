import QtQuick 2.6
import QtQuick.Window 2.2

Window {
    visible: true
    width: 960
    height: 800
    title: qsTr("Fibre")

    MainForm {
        anchors.fill: parent
    }
}
