import QtQuick 2.6
import QtWebView 1.1
import QtWebEngine 1.5

WebEngineView {
    anchors.fill: parent
    url: 'https://app.wire.com/'
}
